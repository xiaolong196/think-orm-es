<?php

namespace think\db\connector;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use think\db\BaseQuery;
use think\db\Connection;
use think\helper\Arr;
use think\db\builder\Elasticsearch as ElasticsearchBuild;
use think\db\Elasticsearch as ElasticsearchQuery;

class Elasticsearch extends Connection
{
    /**
     * @var ElasticsearchBuild
     */
    protected $builder;

    public function connect(array $config = [], $linkNum = 0): Client
    {
        if (isset($this->links[$linkNum])) {
            return $this->links[$linkNum];
        }

        if (empty($config)) {
            $config = $this->config;
        } else {
            $config = array_merge($this->config, $config);
        }

        $this->links[$linkNum] = ClientBuilder::fromConfig($config, true);

        return $this->links[$linkNum];
    }

    public function getBuilderClass()
    {
        return ElasticsearchBuild::class;
    }

    public function getQueryClass(): string
    {
        return ElasticsearchQuery::class;
    }

    public function select(BaseQuery $query): array
    {
        return $this->query($query)['data'];
    }

    public function find(BaseQuery $query): array
    {
        if ($query->getOptions('key')) {
            return $this->connect()->getSource(['index' => $query->getTable(), 'id' => $query->getOptions('key')]);
        }

        return $this->query($query, true)['data'][0] ?? [];
    }

    public function insert(BaseQuery $query, bool $getLastInsID = false)
    {
        $options = $query->getOptions();

        $param = [];
        if (isset($options['data'][$query->getPk()])) {
            $param['id'] = $options['data'][$query->getPk()];
            unset($options['data'][$query->getPk()]);
        }
        $param += ['index' => $query->getTable(), 'body' => $options['data']];
        $result = $this->connect()->index($param);

        return $getLastInsID ? $result['_id'] : 1;
    }

    public function insertAll(BaseQuery $query, array $dataSet = []): int
    {
        $index = $query->getTable();
        $pk = $query->getPk();
        foreach ($dataSet as $data) {
            $params['body'][] = [
                'index' => [
                        '_index' => $index,
                    ] + (isset($data[$pk]) ? ['_id' => $data[$pk]] : [])
            ];

            unset($data[$pk]);
            $params['body'][] = $data;
        }

        $responses = $this->connect()->bulk($params);

        return count(array_filter(
            $responses['items'],
            function ($value) {
                return !empty($value['index']['_shards']['successful']);
            }
        ));
    }

    public function update(BaseQuery $query): int
    {
        $options = $query->getOptions();

        if (isset($options['key'])) {
            $params = [
                'index' => $query->getTable(),
                'id' => $options['key'],
                'body' => [
                    'doc' => $options['data']
                ]
            ];

            $response = $this->connect()->update($params);
            return $response['_shards']['successful'] ?? 0;
        }

        $params = $this->builder->select($query);
        foreach ($options['data'] as $key => $value) {
            $script[] = "ctx._source.$key = params.$key";
        }
        $params['body']['script'] = ['source' => implode(';', $script), 'params' => $options['data']];
        $params['conflicts'] = 'proceed';
        return $this->connect()->updateByQuery($params)['updated'] ?? 0;
    }

    public function delete(BaseQuery $query): int
    {
        if ($query->getOptions('key')) {
            return $this->connect()->delete(['index' => $query->getTable(), 'id' => $query->getOptions('key')])['_shards']['successful'] ?? 0;
        }

        $params = $this->builder->select($query);
        $params['conflicts'] = 'proceed';
        return $this->connect()->deleteByQuery($params)['deleted'] ?? 0;
    }

    public function value(BaseQuery $query, string $field, $default = null)
    {
        return $this->find($query)[$field] ?? $default;
    }

    public function column(BaseQuery $query, $column, string $key = ''): array
    {
        $data = $this->select($query);

        $result = [];
        if (is_string($column)) {
            $column = explode(',', $column);
        }
        foreach ($data as $k => $value) {
            $result[$key ? $value[$key] : $k] = array_search('*', $column) === false ? Arr::only($value, $column) : $value;
        }

        return $result;
    }

    public function transaction(callable $callback)
    {
    }

    public function startTrans()
    {
    }

    public function commit()
    {
    }


    public function rollback()
    {
    }

    public function getLastSql(): string
    {
        return '';
    }

    public function close()
    {
    }

    public function query(BaseQuery $query, bool $one = false)
    {
        $query->parseOptions();

        $data = $this->connect()->search($this->builder->select($query, $one));
        $result = array_map(function ($value) {
            return $value['_source'] + ['_id' => $value['_id']];
        }, $data['hits']['hits'] ?? []);

        return ['count' => $data['hits']['total']['value'], 'data' => $result];
    }
}